library(shinydashboard)
library(rgdal)
library(shiny)
library(dplyr)
library(leaflet)
library(DT)
library(tidyverse)

travel <- data.frame("time" = c("6/20/17 13:32", "6/20/17 13:33", "6/20/17 13:34", "6/20/17 13:35", "6/20/17 13:36", "6/20/17 13:37"),
                     "lat" = c(59.313833, 59.312333, 59.309897, 59.307728, 59.300728, 59.298184),
                     "lon" = c(18.070431, 18.07431, 18.085347, 18.076543, 18.080761, 18.076176),
                     stringsAsFactors = F) %>%
  mutate(
    time = as.POSIXct(time, format = "%m/%d/%y %H:%M")
  )

sidebar <- dashboardSidebar(
  sidebarMenu(
   # menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard")),
    menuItem("Sample Animation", icon = icon("th"), tabName = "kmldata"),
    menuItem("OSM Building Footprints", icon = icon("th"), tabName = "bld"),
    menuItem("Survey Output", icon = icon("th"), tabName = "csvd")
    ))

ui <- dashboardPage(
  dashboardHeader(title = "SWM Pilot"),
  sidebar,
  dashboardBody(
    
    tabItems(
     
      
      tabItem(tabName = "kmldata",
              h2("Sample Animation content"),
              
                fluidPage(
                  sliderInput(inputId = "time", label = "Time", min = min(travel$time), 
                              max = max(travel$time),
                              value = min(travel$time),
                              step=60, # set to increment by 60 seconds, adjust appropriately
                              animate=T),
                  leafletOutput("mymap")
                )
              
      ),
      tabItem(tabName = "bld",
              h2("OSM Building Footprints content"),
              fluidPage(
                leafletOutput("mymap1")
                
              )
      ),
      tabItem(tabName = "csvd",
              h2("Survey Output content"),
              fluidPage(
                leafletOutput("mymap2")
                
              )
      )
    )
    # Boxes need to be put in a row (or column)
    
  )
)

server <- function(input, output, session) {
  points <- reactive({
    travel %>% 
      filter(time == input$time)
  })
  
  history <- reactive({
    travel %>%
      filter(time <= input$time)
  })
  
  output$mymap <- renderLeaflet({
    leaflet() %>%
      addTiles() %>%
      addMarkers(lng = ~lon,
                 lat = ~lat,
                 data = points()) %>%
      addMarkers(lng = ~lon,
                 lat = ~lat,
                 data = history()) %>%
      addPolylines(lng = ~lon,
                   lat = ~lat,
                   data = history())
  })
  output$mymap1 <- renderLeaflet({
    shapeData <- readOGR(".",'buildings_in_hwest')
    #ogrInfo(".",'myGIS')
    leaflet()  %>% addTiles() %>% setView(lng =72.8123989 , lat=19.0545294,zoom=11) %>% addPolygons(data=shapeData,weight=5,col = 'red') %>% addMarkers(lng = -106.363590,lat=31.968483,popup="Hi there")
  })
  output$mymap2 <- renderLeaflet({
    #dunkin <- read.csv("/home/flash/swm_r/bmc.csv", stringsAsFactors=F)
    dunkin <- read.csv("bmc133_2.csv", stringsAsFactors=F)
    
    
    leaflet(dunkin) %>% addTiles('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', 
                                 attribution='Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>') %>% setView(lng =72.8123989 , lat=19.0545294,zoom=13) %>% addCircles(~lon, ~lat, popup=dunkin$Society_Name, weight = 3, radius=20, 
                     color=dunkin$color, stroke = TRUE, fillOpacity = 0.8)
  })
  
}

shinyApp(ui, server)